Mickey Mouse Project

author: Laura Düring\
date: 07.06.2024

A subset of the seurat object was created focusing on the transgenic mice in early and late development. In figure 1 one can see a UMAP of the whole seurat object (left) with the colors indicating the different clusters. On the right only the subset is visible with the colors indicating the four different samples. 1 and 2 represent the late stage, 3 and 4 the early stage.

![figure 1 \| UMAP of the whole seurat object (left) and the subset (right).](figures/umap_final_ID.png)

In multiple clusters differentially expressed marker genes were found that are associated with a progression of alzheimer's disease. In the figure below four of those markers can be seen.\
For instance, C4b is a protein of the complement system that was found to be differentialy expressed in AD patients compared to the controls and is related to Tau pathology. Cst7 is a disease-associated transcriptomic signature in microglial cells and involved in pathways concerning phagocytosis and inflammation. Cd74 codes for a protein in the MHC class II complex, expressed on the surface of immune cells including microglia and a sign of increased microglial activity in a diseased brain. [1-4]

![figure 2 \| Differentially Expressed Marker Genes between early and late stage development of transgenic mice.](figures/AD_marker.png)

The GO analysis for Cluster 4 showed overall an involvement in immunological processes, which coincides with the function of the genes upregulated in cluster 4 such as Cd74, Cst7 which are both markers for microglial cells (figure 3). Nevertheless, since these genes seem to be expressed all over the different clusters microglial cells can not be annotated to cluster 4 for certain (figure 2).

![](figures/plot_gseGO_cl4.png){alt="figure 3 | Gene ontology of cluster 4"}

Conclusion

Multiple upregulated markers were found in various clusters that are associated with Alzheimer's disease and take on different roles in its progression. With more time and computing power, more markers and a full cluster annotation could be achieved for this transcriptome and thus deliver a more substantial image of a expression profile of AD in mice.

References

[1] Mathys, H., Davila-Velderrain, J., Peng, Z. et al. Single-cell transcriptomic analysis of Alzheimer’s disease. Nature **570**, 332–337 (2019). <https://doi.org/10.1038/s41586-019-1195-2>

[2] <https://www.frontiersin.org/articles/10.3389/fncel.2021.814891/full>

[3] <https://web.uri.edu/riinbre/stard5-a-novel-target-in-the-progression-of-alzheimers-disease/>

[4] Baleviciute, A., Keane, L. Sex-specific role for microglial CST7 in Alzheimer’s disease. Nat Rev Immunol **23**, 73 (2023). <https://doi.org/10.1038/s41577-022-00830-0>
