# MickeyMouse - Alzheimer's in mice

# Analysis Day 1

## Step 1: Preprocessing

We created a Seurat object from our dataset for analysis. Treatment information was added to the Seurat object to signify the strain, age and replicates of the sequenced cells.

Mitochondrial expressions were not found or indicated in the dataset. We decided to preprocess the data by trimming feature outliers, keeping features above 2500 and below a 8500 threshold.

![](figures/Violin_nfeatures_2500_8500.jpeg)

## Step 2: Normalization and feature extraction

After normalizing our preprocessed dataset we identfied the top 1000 variable features/genes and labeled the top 10.

![](figures/top10_variable_features.jpeg)

## Step 3: Dimensional reduction & principal component analysis

We performed a principal component analysis (PCA) after further scaling the data. The visualization of the PCA suggests a cutoff at 18 principal components. We used this range for downstream analysis.

![](figures/Elbow_nfeatures_2500_8500.jpeg)

## Step 4: Clustering the cells

Here we used UMAP (Uniform Manifold Approximation and Projection) to cluster the cells. A resolution of 4 was used to obtain a cell cluster number comparable to previous work in the field.

![](figures/UMAP_range18_res4.jpeg)
